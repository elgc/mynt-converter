const destination = document.querySelector("#main-container");

const availableCurrencies = ["AUD", "BGN", "BRL", "CAD", "CHF", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HRK", "HUF",
    "IDR", "ILS", "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PLN", "RON", "RUB", "SEK", "SGD", "THB",
    "TRY", "USD", "ZAR"];

Date.prototype.addDays = function (n) {
    let time = this.getTime();
    let changedDate = new Date(time + (n * 24 * 60 * 60 * 1000));
    this.setTime(changedDate.getTime());
    return this;
};

let Loading = React.createClass({
    render: function () {
        return <div id="div-loading" className="show"></div>
    }
});

let Rates = React.createClass({
    render: function () {
        if (Object.keys(this.props.rates).length > 0) {
            return <section className="row rates colored-block-lg blue">
                <div className="text-center">
                    <span className={"base-flag currency-flag currency-flag-" + this.props.base.toLowerCase()}/>
                </div>
                {this.getDataInsideArray(this.props.rates).map(function (rate) {
                    return <div key={ rate.currency + "-" + rate.rate} className="row text-center extra-rate">
                        <div className="col-md-4"></div>
                        <div className="col-md-2 text-left">
                            <span
                                className={"extra-rate currency-flag currency-flag-" + rate.currency.toLowerCase()}/>&nbsp;
                            <span>{rate.currency}</span>
                        </div>
                        <div className="col-md-2 text-right">
                            <span>{rate.rate}</span>
                        </div>
                        <div className="col-md-4"></div>
                    </div>
                })}
            </section>
        } else {
            return <div></div>
        }
    },

    getDataInsideArray: function () {
        let data = [];
        for (let currency in this.props.rates) {
            data.push(
                {
                    'currency': currency,
                    'rate': this.props.rates[currency]
                }
            )
        }
        return data
    }
});


let Chart = React.createClass({
    componentDidMount: function () {
        let self = this;
        let x_data = [];
        let y_data = [];
        Object.keys(this.props.historical).map(function (date) {
            let splitted = date.split('-');
            x_data.push(String(splitted[2]) + "-" + String(splitted[0]) + "-" + String(splitted[1]))
        });
        x_data = x_data.sort();

        x_data.map(function (date) {
            let splitted = date.split('-');
            y_data.push(self.props.historical[String(splitted[1]) + "-" + String(splitted[2]) + "-" + String(splitted[0])]);
        });

        let data = [
            {
                x: x_data,
                y: y_data,
                type: 'scatter'
            }
        ];

        let layout = {
            title: this.props.base + "/" + this.props.versus,
            font: {
                family: 'Dosis',
                size: 10,
            }
        };

        let extras = {
            showLink: false,
            displaylogo: false,
        };

        Plotly.newPlot(ReactDOM.findDOMNode(this.refs.chart), data, layout, extras);
    },

    render: function () {
        return <section className="row colored-block-np">
            <div className="col-md-3 col-xs-1"></div>
            <div className="col-md-6 col-xs-10">
                <div className="chart" ref="chart"></div>
            </div>
            <div className="col-md-3 col-xs-1"></div>
        </section>
    }
});

let Converter = React.createClass({
    render: function () {
        let self = this;
        return <section className="row converter colored-block-sm green">
            <div className="col-md-3 col-xs-1"></div>
            <div className="col-md-3 col-xs-5">
                <div className="input-group">
                    <div className="input-group-btn">
                        <button type="button" className="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown">
                            <span className="caret"/>
                        </button>
                        <button type="button" className="btn btn-default btn-lg">
                            <span className={"currency-flag currency-flag-" + this.props.base.toLowerCase()}/>
                        </button>
                        <ul className="dropdown-menu">
                            {availableCurrencies.map(function (c) {
                                if (c != self.props.base) {
                                    return <li key={'ac-' + c} className="text-center">
                                        <a href="#" id={"base-" + c} onClick={self.handleDropdownOptionClick}>
                                            <span className={"currency-flag currency-flag-" + c.toLowerCase()}/>
                                            <span>&nbsp;&nbsp;{c}</span>
                                        </a>
                                    </li>
                                }
                            })}
                        </ul>
                    </div>
                    <input id="base-input"
                           className="form-control text-right input-lg"
                           type="number"
                           defaultValue="1"
                           onChange={this.handleInputChange}/>
                </div>
            </div>
            <div className="col-md-3 col-xs-5">
                <div className="input-group">
                    <input id="versus-input"
                           className="form-control input-lg"
                           type="number"
                           defaultValue={this.props.rate}
                           onChange={this.handleInputChange}/>
                    <div className="input-group-btn">
                        <button type="button" className="btn btn-default btn-lg">
                            <span className={"currency-flag currency-flag-" + this.props.versus.toLowerCase()}/>
                        </button>
                        <button type="button" className="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown">
                            <span className="caret"/>
                        </button>
                        <ul className="dropdown-menu dropdown-menu-right">
                            {availableCurrencies.map(function (c) {
                                if (c != self.props.versus) {
                                    return <li key={"versus-" + c} className="text-center">
                                        <a href="#" id={"versus-" + c} onClick={self.handleDropdownOptionClick}>
                                            <span className={"currency-flag currency-flag-" + c.toLowerCase()}/>
                                            <span>&nbsp;&nbsp;{c}</span>
                                        </a>
                                    </li>
                                }
                            })}
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-3 col-xs-1"></div>
        </section>
    },

    handleInputChange: function (event) {
        let self = this;
        if (event.target.id == "base-input") {
            $("#versus-input").val((event.target.value * self.props.rate).toFixed(4));
        } else {
            $("#base-input").val((event.target.value / self.props.rate).toFixed(4));
        }
    },

    handleDropdownOptionClick: function (event) {
        let val = null;
        if (event.target.id == "") {
            val = $(event.target).parent().attr('id')
        } else {
            val = event.target.id
        }

        let splitted = val.split('-');
        this.props.changeCurrency(splitted[0], splitted[1]);
    },
});

let Main = React.createClass({
    getInitialState: function () {
        return {
            isFetching: false,
            base: "EUR",
            versus: "USD",
            rate: 0,
            historical: {},
            rates: {},
            availableCurrencies: []
        }
    },

    componentDidMount: function () {
        this.loadData(this.state.base, this.state.versus);
    },

    render: function () {
        if (this.state.isFetching) {
            return <Loading/>
        } else {
            return <div className="row">
                <Converter base={this.state.base} versus={this.state.versus} rate={this.state.rate}
                           changeCurrency={this.changeCurrency}/>
                <Chart historical={this.state.historical} base={this.state.base} versus={this.state.versus}/>
                <Rates rates={this.state.rates} base={this.state.base}/>
            </div>
        }
    },

    loadData: function (base, versus) {
        this.setState({isFetching: true});

        let self = this;
        let latestRateUrl = "https://serene-fjord-38187.herokuapp.com/api/v1/latest/" + base + "-" + versus;
        let ratesUrl = "https://serene-fjord-38187.herokuapp.com/api/v1/latest/" + base;
        let historicalUrl = latestRateUrl.replace("latest", "historical");
        $.get({
            dataType: " json",
            url: latestRateUrl,
            success: function (response) {
                let rate = parseFloat(response.rate);
                let base = response.base;
                let versus = response.versus;
                $.get({
                    dataType: "json",
                    url: ratesUrl,
                    success: function (r) {
                        let rates = r.rates;

                        let today = new Date();
                        let three_months_back = today.addDays(-31 * 3);
                        three_months_back = String(three_months_back.getMonth() + 1) + "-" + String(three_months_back.getDate()) + "-" + String(three_months_back.getFullYear());
                        today = today.addDays(31 * 3);
                        today = String(today.getMonth() + 1) + "-" + String(today.getDate()) + "-" + String(today.getFullYear());

                        $.get({
                            dataType: "json",
                            url: historicalUrl,
                            data: {
                                start: three_months_back,
                                end: today
                            },
                            success: function (response) {
                                self.setState({
                                    isFetching: false,
                                    rate: parseFloat(rate),
                                    base: base,
                                    versus: versus,
                                    rates: rates,
                                    historical: response.rates
                                })
                            }
                        })
                    }
                })
            }
        })
    },

    changeCurrency: function (side, val) {
        if (side == "base") {
            this.loadData(val, this.state.versus);
        } else {
            this.loadData(this.state.base, val);
        }
    }
});

ReactDOM.render(
    <div>
        <Main/>
    </div>
    ,
    destination
);
